package com.amen.sockets.client;

public class Client {
	public static final String CONST_HOSTNAME = "192.168.0.150";
	public static final Integer CONST_PORT = 10000;

	private SocketClient client;
	
	public Client() {
		client = new SocketClient();
		client.startConnection(CONST_HOSTNAME, CONST_PORT);
	}
	
	public void write(String something){
		client.write(something);
//		System.out.println("Client returned: " + client.read());
	}
}
