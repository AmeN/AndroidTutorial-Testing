package com.amen.sockets.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {

	private PrintWriter writer;
	private BufferedReader reader;
	
	public SocketClient() {
		
	}
	
	public void startConnection(String hostname, Integer port){
		try{
			Socket s = new Socket(hostname, port);
			s.setSoTimeout(5000);
			
			writer = new PrintWriter(s.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			Thread readerThread = new Thread(new ReaderClass());
			readerThread.start();
		}catch (IOException e) {
			System.out.println("Socket did not open.");
		}
	}
	
	public synchronized void write(String something){
		writer.println(something);
		writer.flush();
	}
	
	public class ReaderClass implements Runnable{
		@Override
		public void run() {
			read();
		}
		
		public void read(){
			while(true){	
				try{
					if(reader.ready()){
						System.out.println(reader.readLine());
					}else{
						Thread.sleep(100);
					}
					
				}catch (IOException e) {
					System.out.println("Nothing in return");
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
	}
}
