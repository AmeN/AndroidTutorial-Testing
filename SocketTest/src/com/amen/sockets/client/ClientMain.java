package com.amen.sockets.client;

import java.util.Scanner;

public class ClientMain {

	public static void main(String[] args) {
		
		Client c = new Client();
		Scanner sc = new Scanner(System.in);
		
		String line = sc.nextLine();
		while (!line.equals("not")){
			c.write(line);
			line = sc.nextLine();
		}
	}

}
