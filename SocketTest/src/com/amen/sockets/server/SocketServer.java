package com.amen.sockets.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SocketServer implements Observer {
	public static final Integer CONST_PORT = 10000;
	public static Boolean SERVER_WORKING = true;
	private ServerSocket server;

	private HashMap<String, SocketServerWorker> workers;

	public SocketServer() {
		workers = new HashMap<>();
	}

	public void addNewWorker(SocketServerWorker newWorker) {
		workers.put(newWorker.getSocket().getInetAddress().getHostAddress(), newWorker);
		newWorker.addObserver(this);
	}

	public synchronized void broadcastMessage(String message, SocketServerWorker sender) {
		String ip = sender.getSocket().getInetAddress().getHostAddress();
		message = ip + " sent:" + message;

		Iterator<SocketServerWorker> it = workers.values().iterator();
		while (it.hasNext()) {
			SocketServerWorker worker = it.next();
			if (worker.getSocket().isClosed()) {
				workers.remove(worker.getSocket().getInetAddress().getHostAddress());
				continue;
			}

			if (!worker.equals(sender)) {
				worker.sendMessage(message);
			}
		}
	}

	// spots123
	public void listen() {
		try {
			server = new ServerSocket(CONST_PORT);
			server.setSoTimeout(10000);
		} catch (IOException e) {
			System.out.println("Socket server didnt open");
		}

		while (SERVER_WORKING) {
			try {
				Socket opened = server.accept();
				if (!opened.isClosed()) {

					SocketServerWorker newWorker = new SocketServerWorker(opened);
					addNewWorker(newWorker);

					System.out.println(
							"opened port- accepted from: " + newWorker.getSocket().getInetAddress().getHostAddress());

					Thread th = new Thread(newWorker);
					th.start();
				}
			} catch (SocketTimeoutException ste) {
				System.out.println("No new connections");
			} catch (IOException e) {
				System.out.println("Error from reading.");
			}
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		String message = null;
		if (arg1 instanceof String) {
			message = (String) arg1;
		}

		SocketServerWorker sender = null;
		if (arg0 instanceof SocketServerWorker) {
			sender = (SocketServerWorker) arg0;
		}

		System.out.println("something has been written : " + message);
		broadcastMessage(message, sender);
	}
}
