package com.amen.sockets.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Observable;
import java.util.Observer;

public class SocketServerWorker extends Observable implements Runnable {

	private BufferedReader reader;
	private PrintWriter writer;
	
	private Socket socket;

	public SocketServerWorker(Socket pSocket) {
		try {
			socket = pSocket;
			socket.setSoTimeout(5000);
			writer = new PrintWriter(pSocket.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(pSocket.getInputStream()));
		} catch (IOException e) {
			System.out.println("Socket did not open.");
		}
	}
	
	public Socket getSocket() {
		return socket;
	}

	@Override
	public void run() {
		while (SocketServer.SERVER_WORKING) {
			//->
			try {
				if (!reader.ready()) {
					Thread.sleep(100);
					continue;//->
				}
				String line = reader.readLine();
				if (line != null) {
					setChanged();
					notifyObservers(line);
				} else {
					System.out.println("empty line");
				}
			} catch (SocketTimeoutException ste) {
				System.out.println("Nothing happening");
			} catch (IOException e) {
				System.out.println("Can't read/write");
			}catch (InterruptedException e) {
				// TODO: handle exception
			}
		}
	}

	public void sendMessage(String message) {
		writer.println(message);
		writer.flush();
	}
}
