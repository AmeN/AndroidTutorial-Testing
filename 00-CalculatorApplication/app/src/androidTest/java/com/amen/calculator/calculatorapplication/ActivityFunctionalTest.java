package com.amen.calculator.calculatorapplication;

import android.support.test.espresso.core.deps.guava.util.concurrent.Runnables;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.UiThreadTest;
import android.view.KeyEvent;
import android.widget.EditText;

import org.junit.Test;

/**
 * Created by amen on 2/6/17.
 */

public class ActivityFunctionalTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public ActivityFunctionalTest() {
        super(MainActivity.class);
    }

    @Test
    public void testSetText() throws Exception {
        MainActivity activity = getActivity();

        final EditText editText = (EditText) activity.findViewById(R.id.editText);

        Runnable x = new Runnable() {

            @Override
            public void run() {
//                TouchUtils.clickView(this, editText);
                editText.setText("Wrong value!");
            }
        };
//        new Thread (x);
        // set text
        getActivity().runOnUiThread(x);

        getInstrumentation().waitForIdleSync();
        assertEquals("Wrong value!", "Wrong value!", editText.getText().toString());
    }

    @UiThreadTest
    public void testSetTextWithAnnotation() throws Exception {
        MainActivity activity = getActivity();

        final EditText editText = (EditText) activity.findViewById(R.id.editText);

//        sendKeys(KeyEvent.KEYCODE_BUTTON_8);

        editText.setText("Wrong value!");
        assertEquals("Wrong value!", "Wrong value!", editText.getText().toString());
    }


}
