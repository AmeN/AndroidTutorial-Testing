package com.amen.calculator.calculatorapplication;

/**
 * Created by amen on 2/6/17.
 */

public class CalculatorInstantiable {

    private static String SIGN_MUL = "*";
    private static String SIGN_SUB = "-";
    private static String SIGN_ADD = "+";
    private static String SIGN_DIV = "/";

    public boolean validateOperations(String line) {
        int foundSign = 0;
        if (line.contains(SIGN_MUL)) {
            foundSign++;
        }

        if (line.contains(SIGN_ADD)) {
            foundSign++;
        }

        if (line.contains(SIGN_DIV)) {
            foundSign++;
        }

        if (line.contains(SIGN_SUB)) {
            foundSign++;
        }

        if (foundSign != 1) {
            return false;
        }
        return true;
    }

    public double calculate(String line) throws Exception {
        if (!validateOperations(line) ) {
            throw new Exception("invalid equation");
        }

        if (line.contains(SIGN_MUL)) {
            return parseAndCountMul(line);
        } else if (line.contains(SIGN_ADD)) {
            return parseAndCountAdd(line);
        } else if (line.contains(SIGN_DIV)) {
            return parseAndCountDiv(line);
        } else {
            return parseAndCountSub(line);
        }
    }

    public double parseAndCountSub(String line) {
        String[] splits = line.split("\\"+SIGN_SUB);

        double first = Double.parseDouble(splits[0]);
        double second = Double.parseDouble(splits[1]);

        return substract(first, second);
    }

    public double parseAndCountDiv(String line) throws Exception {
        String[] splits = line.split("\\"+SIGN_DIV);

        double first = Double.parseDouble(splits[0]);
        double second = Double.parseDouble(splits[1]);

        if (second == 0.0) {
            throw new Exception("Can't divide by 0.");
        }

        return divide(first, second);
    }

    public double parseAndCountAdd(String line) {
        String[] splits = line.split("\\"+SIGN_ADD);

        double first = Double.parseDouble(splits[0]);
        double second = Double.parseDouble(splits[1]);

        return add(first, second);
    }

    public double parseAndCountMul(String line) {
        String[] splits = line.split("\\"+SIGN_MUL);

        double first = Double.parseDouble(splits[0]);
        double second = Double.parseDouble(splits[1]);

        return multiply(first, second);
    }

    public double add(double first, double second) {
        return first + second;
    }

    public double divide(double first, double second) {
        return first / second;
    }

    public double multiply(double first, double second) {
        return first * second;
    }

    public double substract(double first, double second) {
        return first - second;
    }
}
