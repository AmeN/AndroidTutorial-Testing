package com.amen.calculator.calculatorapplication;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by amen on 2/7/17.
 */

public class CalculatorValidatorTests {

    private static CalculatorInstantiable calculator;

    @BeforeClass
    public static void createCalculator() {
        calculator = new CalculatorInstantiable();
    }

    @Test
    public void testValidationOK(){
        Assert.assertTrue(calculator.validateOperations("20+30"));
        Assert.assertTrue(calculator.validateOperations("200+"));
        Assert.assertTrue(calculator.validateOperations("2+030"));
        Assert.assertTrue(calculator.validateOperations("+20+30"));
        Assert.assertTrue(calculator.validateOperations("+"));

        Assert.assertFalse(calculator.validateOperations("20"));
        Assert.assertFalse(calculator.validateOperations(""));
        Assert.assertFalse(calculator.validateOperations("++"));
    }

    @Test
    public void testValidationManyOperations(){
        Assert.assertFalse(calculator.validateOperations("20+30+"));
        Assert.assertFalse(calculator.validateOperations("200+-"));
        Assert.assertFalse(calculator.validateOperations("2+030/"));
        Assert.assertFalse(calculator.validateOperations("+20+30---"));
        Assert.assertFalse(calculator.validateOperations("++-"));
    }
}
