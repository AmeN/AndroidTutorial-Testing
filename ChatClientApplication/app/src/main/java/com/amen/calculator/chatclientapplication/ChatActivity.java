package com.amen.calculator.chatclientapplication;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatActivity extends AppCompatActivity implements Inotifiable, Runnable {

    @BindView(R.id.statusText)
    TextView statusText;

    @BindView(R.id.chatContent)
    TextView content;

    @BindView(R.id.messageField)
    EditText messageField;

    private Handler handler;

    @OnClick(R.id.buttonSend)
    public void sendToServer(View v) {
        client.write(messageField.getText().toString());
        addMessage("You:    " + messageField.getText().toString());
        messageField.setText("");
    }

    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        handler = new Handler();
        new Thread(this).start();
    }

    @Override
    public void run() {
        String ip = getIntent().getStringExtra("IP");
        client = new Client(ip);
        client.setNotifiable(this);

        handler.postDelayed(mStatusChecker, 1000);
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateStatus(); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                handler.postDelayed(mStatusChecker, 1000);
            }
        }
    };

    public void updateStatus() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (client.isConnected()) {
                    statusText.setText("Status: Connected");
                } else {
                    statusText.setText("Status: Disconnected");
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(mStatusChecker);
        super.onDestroy();
    }

    @Override
    public void addMessage(String message) {
        content.setText(content.getText() + "\n" + message);
    }

}
