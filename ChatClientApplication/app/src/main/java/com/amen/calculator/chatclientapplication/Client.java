package com.amen.calculator.chatclientapplication;

/**
 * Created by amen on 2/9/17.
 */

public class Client {
    public static final Integer CONST_PORT = 10000;

    private SocketClient client;
    public Client(String hostname) {
        client = new SocketClient();
        client.startConnection(hostname, CONST_PORT);
    }

    public void write(String something){
        client.write(something);
//		System.out.println("Client returned: " + client.read());
    }

    public void stopSocket(){
        client.stopReader();
    }

    public void setNotifiable(Inotifiable notifiable) {
        client.setNotifiable(notifiable);
    }

    public boolean isConnected() {
        return client.isConnected();
    }
}