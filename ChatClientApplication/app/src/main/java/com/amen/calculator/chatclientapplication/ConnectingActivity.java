package com.amen.calculator.chatclientapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConnectingActivity extends AppCompatActivity {

    @BindView(R.id.ipField)
    public EditText ipField;

    @OnClick(R.id.connectButton)
    public void connectToServer() {
        Intent i = new Intent(this, ChatActivity.class);
        i.putExtra("IP", ipField.getText().toString());
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connecting);
        ButterKnife.bind(this);
    }
}
