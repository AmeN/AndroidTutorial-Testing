package com.amen.calculator.chatclientapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by amen on 2/9/17.
 */

public class SocketClient {

    private PrintWriter writer;
    private BufferedReader reader;
    private boolean isClientWorking = false;
    private Inotifiable notifiable;

    public SocketClient() {
        isClientWorking = true;
    }

    private Socket socket;

    public void startConnection(String hostname, Integer port) {
        try {
            socket = new Socket(hostname, port);
            socket.setSoTimeout(5000);

            writer = new PrintWriter(socket.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            Thread readerThread = new Thread(new ReaderClass());
            readerThread.start();
        } catch (IOException e) {
            System.out.println("Socket did not open.");
        }
    }

    public void stopReader() {
        isClientWorking = false;
    }

    public synchronized void write(String something) {
        writer.println(something);
        writer.flush();
    }

    public void setNotifiable(Inotifiable notifiable) {
        this.notifiable = notifiable;
    }

    public boolean isConnected() {
        return !socket.isInputShutdown() && !socket.isOutputShutdown();
    }

    public class ReaderClass implements Runnable {
        @Override
        public void run() {
            read();
        }

        public void read() {
            while (isClientWorking) {
                try {
                    if (reader.ready()) {
                        notifiable.addMessage(reader.readLine());
                    } else {
                        Thread.sleep(100);
                    }

                } catch (IOException e) {
                    System.out.println("Nothing in return");
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }
    }
}