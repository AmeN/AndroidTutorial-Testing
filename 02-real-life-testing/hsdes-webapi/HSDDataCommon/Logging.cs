﻿using System;

namespace HSDDataCommon
{
    /// <summary>
    /// Centralized class for logging messages from applications. It may be changed to use any kind of logger library.
    /// </summary>
    public static class Logging
    {
        /// <summary>
        /// Logs informational message.
        /// </summary>
        public static void LogInfo(string msg)
        {
            Console.WriteLine(PrepareMessage(msg));
        }

        /// <summary>
        /// Logs error message.
        /// </summary>
        /// <param name="msg">Message to be logged.</param>
        public static void LogError(string msg)
        {
            Console.Error.WriteLine(PrepareMessage(msg));
        }

        /// <summary>
        /// Adds date-time prefix to the messge being logged.
        /// </summary>
        /// <param name="msg">Original message</param>
        /// <returns>Extended message.</returns>
        private static string PrepareMessage(string msg)
        {
            return string.Format("[{0}] {1}", DateTime.Now, msg);
        }
    }
}
