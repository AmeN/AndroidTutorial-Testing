﻿namespace HSDDataCommon
{
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;

    /// <summary>
    /// Class for searching usernames in the Active Directory.
    /// </summary>
    public class ActiveDirectoryHelper : IActiveDirectoryHelper
    {
        /// <summary>
        /// List of Active Directory domains to search in
        /// </summary>
        private static List<String> domains = new List<string>() { "ccr.corp.intel.com", "ger.corp.intel.com", "amr.corp.intel.com", "gar.corp.intel.com" };

        /// <summary>
        /// Found aliases cache. Maps two kind of key-value pairs:
        /// <list type="bullet">
        /// <item>[email]->[user alias|null]: mapping from email to respective user alias or null if alias not found</item>
        /// <item>[user alias]->[true|false]: mapping from user alias to "true" if found or "false" if not found</item>
        /// </list>
        /// </summary>
        private Dictionary<string, string> existingPeople = new Dictionary<string, string>();

        /// <summary>
        /// Finds user's alias in Active Directory based on email address.
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns>Alias or null if not found.</returns>
        public string FindUserAliasByEmail(string userEmail)
        {
            lock (existingPeople)
            {
                if (existingPeople.ContainsKey(userEmail))
                {
                    return existingPeople[userEmail];
                }
            }

            foreach (var domain in domains)
            {

                var ret = FindEmailInDomain(userEmail, domain);
                if (!string.IsNullOrEmpty(ret))
                {
                    lock (existingPeople)
                    {
                        existingPeople[userEmail] = ret;
                    }

                    return ret;
                }
            }

            lock (existingPeople)
            {
                existingPeople[userEmail] = null;
            }

            return null;
        }

        /// <summary>
        /// Checks if given user alias exists in the Active Directory database.
        /// </summary>
        /// <param name="alias">User alias to be checked</param>
        /// <returns>true if alias exists; otherwise, false</returns>
        public bool UserAliasExists(string alias)
        {
            lock (existingPeople)
            {
                if (existingPeople.ContainsKey(alias))
                {
                    return existingPeople[alias].Equals("true");
                }
            }

            foreach (var domain in domains)
            {
                if (FindAliasInDomain(alias, domain))
                {
                    lock (existingPeople)
                    {
                        existingPeople[alias] = "true";
                    }

                    return true;
                }
            }

            lock (existingPeople)
            {
                existingPeople[alias] = "false";
            }

            return false;
        }

        /// <summary>
        /// Finds user's alias in Active Directory (in given domain) based on email address.
        /// </summary>
        /// <returns>Alias or null if not found.</returns>
        private static string FindEmailInDomain(string userEmail, string domainName)
        {
            PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, domainName);
            UserPrincipal user = new UserPrincipal(domainContext);

            user.EmailAddress = userEmail;
            PrincipalSearcher pS = new PrincipalSearcher();
            pS.QueryFilter = user;
            PrincipalSearchResult<Principal> results = pS.FindAll();
            var list = results.ToList();
            if (list == null || list.Count == 0)
            {
                return null;
            }

            Principal pc = list[0];
            DirectoryEntry de = (DirectoryEntry)pc.GetUnderlyingObject();
            return pc.SamAccountName;
        }

        /// <summary>
        /// Checks if user's alias is present in Active Directory (in given domain).
        /// </summary>
        /// <returns>true if alias excsts; otherwise, false.</returns>
        private static bool FindAliasInDomain(string alias, string domainName)
        {
            PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, domainName);
            UserPrincipal user = new UserPrincipal(domainContext);

            user.SamAccountName = alias;
            PrincipalSearcher pS = new PrincipalSearcher();
            pS.QueryFilter = user;
            PrincipalSearchResult<Principal> results = pS.FindAll();
            var list = results.ToList();
            if (list == null || list.Count == 0)
            {
                return false;
            }

            return true;
        }
    }
}