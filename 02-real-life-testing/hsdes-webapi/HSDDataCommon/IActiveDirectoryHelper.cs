﻿namespace HSDDataCommon
{
    public interface IActiveDirectoryHelper
    {
        /// <summary>
        /// Finds user's alias in Active Directory based on email address.
        /// </summary>
        /// <param name="userEmail"></param>
        /// <returns>Alias or null if not found.</returns>
        string FindUserAliasByEmail(string userEmail);


        /// <summary>
        /// Checks if given user alias exists in the Active Directory database.
        /// </summary>
        /// <param name="alias">User alias to be checked</param>
        /// <returns>true if alias exists; otherwise, false</returns>
        bool UserAliasExists(string alias);
    }
}
