﻿
namespace HSDDataCommon.Notes
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;


    [Serializable]
    public class CorruptedNotesException : ApplicationException
    {
        public string RawText { get; set; }

        public CorruptedNotesException()
        {
        }

        public CorruptedNotesException(string message)
            : base(message)
        {

        }
        public CorruptedNotesException(string message,
      Exception innerException)
            : base(message, innerException)
        {
        }

        protected CorruptedNotesException(SerializationInfo info,
      StreamingContext context)
            : base(info, context)
        {
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("RawText", RawText);
        }
    }
}
