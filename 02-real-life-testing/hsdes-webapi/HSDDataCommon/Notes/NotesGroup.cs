﻿using System;
using System.Collections.Generic;

namespace HSDDataCommon.Notes
{
    /// <summary>
    /// Group of HSD/HSDES comments related to a single bug.
    /// </summary>
    public class NotesGroup
    {
        /// <summary>
        /// HSD Classic id of the bug.
        /// </summary>
        public uint ClassicId { get; set; }

        /// <summary>
        /// List of notes.
        /// </summary>
        public List<Note> Notes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public NotesGroup()
        {
            Notes = new List<Note>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw">Raw string containing all notes as exported from HSD Classic.</param>
        /// <param name="classicId">Id of bug owning the notes in HSD Classic.</param>
        public NotesGroup(string raw, uint classicId, IActiveDirectoryHelper adHelper)
        {
            this.ClassicId = classicId;
            Notes = new List<Note>();

            Logging.LogInfo("Parsing notes for classic id: " + classicId);
            try
            {
                var parsed = Note.ParseNotesString(raw, adHelper);
                for (int i = 0; i < parsed.Count; i++)
                {
                    Notes.Add(parsed[i]);
                }
            }
            catch (Exception)
            {
                Logging.LogError("Could not parse notes for classic id: " + classicId);
                throw;
            }
        }
    }
}
