﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace HSDDataCommon.Notes
{
    /// <summary>
    /// Single comment to a HSD / HSDES bug.
    /// </summary>
    public class Note
    {
        private const int NOTE_LOG_LENGTH = 200;

        /// <summary>
        /// Date when note was originally published in Classic HSD
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Author of the note.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Contents of the note.
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Note()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawText">Raw text of the note to be parsed.</param>
        public Note(string rawText, IActiveDirectoryHelper adHelper)
        {
            string headerRegex = @"^(\w+)\s+(.+)";
            var myRegex = new Regex(headerRegex);
            Match match = myRegex.Match(rawText);

            if (match.Groups.Count == 3)
            {
                this.Author = match.Groups[1].Value;
                string dateStr = match.Groups[2].Value;

                DateTime parsedDate = DateTime.Now;

                if (!DateTime.TryParse(dateStr, out parsedDate))
                {
                    throw new ApplicationException("Date could not be parsed");
                }

                this.Date = parsedDate;
                if (!adHelper.UserAliasExists(this.Author))
                {
                    throw new ApplicationException("Note's author does not exist.");
                }

            }
            else
            {
                throw new ApplicationException("Note could not be parsed");
            }

            int headerEnd = rawText.IndexOf('\n');

            if (headerEnd >= 0)
            {
                this.Contents = rawText.Substring(headerEnd + 1).Trim();
            }
            else
            {
                this.Contents = rawText;
            }
        }

        /// <summary>
        /// Parses raw string containing a number of notes from Classic HSD.
        /// </summary>
        /// <param name="raw">String to be parsed</param>
        /// <returns>Collection of Note objects as parsed from the input raw string.</returns>
        public static IList<Note> ParseNotesString(string raw, IActiveDirectoryHelper adHelper)
        {
            var ret = new List<Note>();

            if (String.IsNullOrEmpty(raw))
            {
                return ret;
            }

            var rawNotes = raw.Split(new string[] { "+++++" }, StringSplitOptions.RemoveEmptyEntries);

            var unparsableNotes = new Dictionary<String, Int32>();

            for (int i = 0; i < rawNotes.Length; i++)
            {
                var rawNote = rawNotes[i];

                if (!string.IsNullOrWhiteSpace(rawNote))
                {
                    try
                    {
                        ret.Add(new Note(rawNote.Trim(), adHelper));
                    }
                    catch (Exception ex)
                    {
                        unparsableNotes[rawNote] = ret.Count;
                        Logging.LogError("Below note could not be processed: " + ex.Message);
                        Logging.LogError("It will be attached to previous note's text as unparsed content!");
                        DumpToLog(rawNote);
                        Logging.LogError(String.Empty);
                    }
                }
            }

            HandleUnparsables(raw, ret, unparsableNotes);
            return ret;
        }

        /// <summary>
        /// Takes care of notes that could not be parsed. Each such note is attached to previous one that was parsed correctly.
        /// If no single note was parsed correctly, 
        /// </summary>
        /// <param name="raw">Raw string containing all notes</param>
        /// <param name="parsedNodes">List of already parsed notes</param>
        /// <param name="unparsableNotes">Maps unparsable notes to respective indexes of parsed notes, that each unparsable should be attached to.</param>
        /// <exception cref="CorruptedNotesException">Thrown if no single note could be parsed.</exception>
        private static void HandleUnparsables(string raw, List<Note> parsedNodes, Dictionary<string, int> unparsableNotes)
        {
            if (unparsableNotes.Count == 0)
            {
                return;
            }

            if (parsedNodes.Count == 0)// there are no correctly parsed notes to attach unparsables to. All notes are unparsable.
            {
                Logging.LogError("Whole notes string could not be parsed. Raw comments will be added to the description.");
                DumpToLog(raw);
                var ex = new CorruptedNotesException();
                ex.RawText = raw;
                throw ex;
            }

            foreach (string rawWrong in unparsableNotes.Keys)
            {
                int idx = unparsableNotes[rawWrong];
                if (idx > 0)
                {
                    idx--;
                }

                parsedNodes[idx].Contents += string.Concat("\n++++++\n", rawWrong);
            }
        }

        /// <summary>
        /// Prints text of note to the error log.
        /// </summary>
        /// <param name="rawNote">Note's text</param>
        private static void DumpToLog(string rawNote)
        {
            Logging.LogError("----------------- RAW TEXT -------------------");
            Logging.LogError(FormatNoteForLog(rawNote, NOTE_LOG_LENGTH));
            Logging.LogError("----------------------------------------------");
        }

        /// <summary>
        /// Adds '>' to the beginning of each line of the note and truncates it if it is too long.
        /// </summary>
        /// <param name="str">Note's text</param>
        /// <param name="length">Max length of raw text. If exceeded, note will be truncated.</param>
        /// <returns>Formatted text</returns>
        private static string FormatNoteForLog(string str, int length)
        {
            if (str.Length <= length)
            {
                return str.Replace("\n", "\r\n>\t");
            }

            string ellipsis = "(...)";
            return str.Substring(0, length - ellipsis.Length).Replace("\n", "\r\n>\t") + ellipsis;
        }
    }

}
