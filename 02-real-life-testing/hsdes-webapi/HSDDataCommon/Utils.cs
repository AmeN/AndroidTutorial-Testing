﻿namespace HSDDataCommon
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    public static class Utils
    {
        /// <summary>
        /// Creates given directory. On error, logs message to logger.
        /// </summary>
        /// <param name="directory">Path to directory to be created.</param>
        public static void CreateDirectory(string directory)
        {
            try
            {
                Directory.CreateDirectory(directory);
            }
            catch (Exception)
            {
                Logging.LogError("Could not create output directory: " + directory);
                throw;
            }
        }

        /// <summary>
        /// Serializes given object to xml.
        /// </summary>
        /// <param name="data">Object to be serialized.</param>
        /// <param name="outFile">Path to output xml file.</param>
        public static void SerializeToXml(Object data, string outFile)
        {
            XmlSerializer x = new XmlSerializer(data.GetType());

            try
            {
                using (TextWriter writer = new StreamWriter(outFile))
                {
                    x.Serialize(writer, data);
                }
            }
            catch (Exception)
            {
                Logging.LogError("Error when saving xml file: " + outFile);
                throw;
            }
        }

        /// <summary>
        /// Writes xsd file for given xml input file.
        /// </summary>
        /// <param name="xmlInput">Path of input xml file from which xsd should be generated.</param>
        public static void WriteSchemaOfXML(List<string> columns, string dataTableName, string xmlInput)
        {
            DataTable table = new DataTable(dataTableName);
            foreach(string column in columns) {
                table.Columns.Add(column);
            }

            table.WriteXmlSchema((xmlInput.Replace(".xml", "") + ".xsd"));
        }
    }
}
