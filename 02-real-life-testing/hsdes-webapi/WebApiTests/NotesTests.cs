﻿namespace WebApiTests
{
    using HSDDataCommon;
    using HSDDataCommon.Notes;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    [TestClass]
    public class NotesTests
    {

        private static List<string> aliases = new List<string>() { "pkiser", "mnarwojx", "greglinx" };

        private Mock<IActiveDirectoryHelper> adHelper = CreateAdHelperMock();

        private static Mock<IActiveDirectoryHelper> CreateAdHelperMock()
        {
            var mock = new Mock<IActiveDirectoryHelper>();
            mock.Setup(ad => ad.FindUserAliasByEmail(It.IsAny<string>())).Returns("user");
            mock.Setup(ad => ad.UserAliasExists(It.IsAny<string>())).Returns((string s) => { return aliases.Contains(s); });
            return mock;
        }


        [TestMethod]
        public void TestNote()
        {
            var adHelper = CreateAdHelperMock();

            var noteString = @"pkiser 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.
";

            Note n = new Note(noteString, adHelper.Object);
            Assert.AreEqual("pkiser", n.Author);
            Assert.AreEqual(2015, n.Date.Year);
            Assert.AreEqual(30, n.Date.Day);
            Assert.AreEqual(1, n.Date.Month);
            Assert.IsTrue(n.Contents.StartsWith("Die Sparing is not supported until Elk Valley"));


            noteString = @"mnarwojx 1/2/2015 01:01:17 AM

Note test.
";

            n = new Note(noteString, adHelper.Object);
            Assert.AreEqual("mnarwojx", n.Author);
            Assert.AreEqual(2015, n.Date.Year);
            Assert.AreEqual(2, n.Date.Day);
            Assert.AreEqual(1, n.Date.Month);
            Assert.IsTrue(n.Contents.StartsWith("Note test."));
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailedParsing3()
        {
            var noteString = @"XXxxXX 1/2/2015 01:01:17 AM

Note test.
";
            var n = new Note(noteString, adHelper.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailedParsing2()
        {
            var noteString = @"1/2/201

Note test.
";
            var n = new Note(noteString, adHelper.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailedParsing()
        {
            var noteString = @"1/2/2015 01:01:17 AM

Note test.
";
            var n = new Note(noteString, adHelper.Object);
        }

        [TestMethod]
        public void TestManyNotesCorrect()
        {
            var adHelper = CreateAdHelperMock();

            var rawString = @"

+++++ pkiser 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.

+++++ pkiser 9/21/2015 4:54:00 PM

Accepted as a defect

+++++ mnarwojx 10/27/2015 6:22:46 PM

This one needs to be retested. I have checked the flag ENABLE_FW_STATE_NVDATA_ACCESS, which is set same to 0 in POC build and in Nano core sim build. This means there is no possible problem in FW states data loading issue from SXP or SPI. And then I run the test on simulator, the agrresiveness is set to default value 0x80 correctly.

+++++ pkiser 2/8/2016 4:44:29 PM

Please retest with the latest fw.";

            var result = Note.ParseNotesString(rawString, adHelper.Object);

            Assert.AreEqual(4, result.Count);
            Assert.AreEqual("pkiser", result[0].Author);
            Assert.AreEqual("pkiser", result[1].Author);
            Assert.AreEqual("mnarwojx", result[2].Author);
            Assert.AreEqual("pkiser", result[3].Author);

            rawString = @"

+++++ pkiser 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.
";

            result = Note.ParseNotesString(rawString, adHelper.Object);

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("pkiser", result[0].Author);

        }

        [TestMethod]
        public void TestManyNotesWithWrong()
        {
            var adHelper = CreateAdHelperMock();

            var rawString = @"

+++++ pkiser 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.

+++++ pkiser 9/21/2015 4:54:00 PM

Accepted as a defect

+++++ XXxxXX 10/27/2015 6:22:46 PM

This one needs to be retested. I have checked the flag ENABLE_FW_STATE_NVDATA_ACCESS, which is set same to 0 in POC build and in Nano core sim build. This means there is no possible problem in FW states data loading issue from SXP or SPI. And then I run the test on simulator, the agrresiveness is set to default value 0x80 correctly.

+++++ greglinx 2/8/2016 4:44:29 PM

Please retest with the latest fw.";

            var result = Note.ParseNotesString(rawString, adHelper.Object);
            Assert.AreEqual(3, result.Count, "wrong note should not be parsed");
            Assert.AreEqual("pkiser", result[0].Author, "First note is OK");
            Assert.AreEqual("pkiser", result[1].Author, "Second note is OK");
            Assert.AreEqual("greglinx", result[2].Author, "Third note is last");

            Assert.IsTrue(result[1].Contents.StartsWith("Accepted as a defect"), "Second note should start as original one.");
            Assert.IsTrue(result[1].Contents.Contains("XXxxXX 10/27/2015 6:22:46 PM"), "Second note should include wrong one");
            Assert.IsTrue(result[1].Contents.TrimEnd().EndsWith("value 0x80 correctly."), "Second not should end with wrong one.");
        }

        [TestMethod]
        public void TestManyNotesWithWrong2()
        {
            var adHelper = CreateAdHelperMock();

            var rawString = @"

+++++ pkiser 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.

+++++ pkiser 9/21/2015 4:54:00 PM

Accepted as a defect

+++++ XXxxXX 10/27/2015 6:22:46 PM

This one needs to be retested. I have checked the flag ENABLE_FW_STATE_NVDATA_ACCESS, which is set same to 0 in POC build and in Nano core sim build. This means there is no possible problem in FW states data loading issue from SXP or SPI. And then I run the test on simulator, the agrresiveness is set to default value 0x80 correctly.

+++++ 9/21/201

Please retest with the latest fw.";

            var result = Note.ParseNotesString(rawString, adHelper.Object);
            Assert.AreEqual(2, result.Count, "wrong notes should not be parsed");
            Assert.AreEqual("pkiser", result[0].Author, "First note is OK");
            Assert.AreEqual("pkiser", result[1].Author, "Second note is OK");

            Assert.IsTrue(result[1].Contents.StartsWith("Accepted as a defect"), "Second note should start as original one.");
            Assert.IsTrue(result[1].Contents.Contains("XXxxXX 10/27/2015 6:22:46 PM"), "Second note should include wrong one");
            Assert.IsTrue(result[1].Contents.TrimEnd().EndsWith("Please retest with the latest fw."), "Second not should end with second wrong one.");
        }

        [TestMethod]
        public void TestManyNotesWithWrong3()
        {
            var adHelper = CreateAdHelperMock();

            var rawString = @"

+++++ XXxxXX 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.

+++++ pkiser 9/21/2015 4:54:00 PM

Accepted as a defect

+++++ mnarwojx 10/27/2015 6:22:46 PM

This one needs to be retested. I have checked the flag ENABLE_FW_STATE_NVDATA_ACCESS, which is set same to 0 in POC build and in Nano core sim build. This means there is no possible problem in FW states data loading issue from SXP or SPI. And then I run the test on simulator, the agrresiveness is set to default value 0x80 correctly.

+++++ greglinx 2/8/2016 4:44:29 PM

Please retest with the latest fw.";

            var result = Note.ParseNotesString(rawString, adHelper.Object);
            Assert.AreEqual(3, result.Count, "wrong note should not be parsed");
            Assert.AreEqual("pkiser", result[0].Author, "First note from pkiser");
            Assert.AreEqual("mnarwojx", result[1].Author, "Second note from mnarwojx");
            Assert.AreEqual("greglinx", result[2].Author, "Third note from greglinx");

            Assert.IsTrue(result[0].Contents.StartsWith("Accepted as a defect"), "First note should start as first correct one.");
            Assert.IsTrue(result[0].Contents.Contains("XXxxXX 1/30/2015 8:25:17 PM"), "First note should include wrong one");
            Assert.IsTrue(result[0].Contents.TrimEnd().EndsWith("Die Sparing is not supported until Elk Valley."), "First not should end with wrong one.");
        }

        [TestMethod]
        [ExpectedException(typeof(CorruptedNotesException))]
        public void TestAllWrongNotes()
        {
            var adHelper = CreateAdHelperMock();

            var rawString = @"

+++++ XXxxXX 1/30/2015 8:25:17 PM

Die Sparing is not supported until Elk Valley.

+++++ XXxXxx 9/21/201
Accepted as a defect

+++++ 10/27/201

This one needs to be retested. I have checked the flag ENABLE_FW_STATE_NVDATA_ACCESS, which is set same to 0 in POC build and in Nano core sim build. This means there is no possible problem in FW states data loading issue from SXP or SPI. And then I run the test on simulator, the agrresiveness is set to default value 0x80 correctly.

+++++  10/27/201

Please retest with the latest fw.";

            var result = Note.ParseNotesString(rawString, adHelper.Object);
        }
    }
}
