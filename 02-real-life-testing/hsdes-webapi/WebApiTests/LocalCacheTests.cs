﻿namespace WebApiTests
{
    using HSDESWebApi;
    using HSDESWebApi.Messages;
    using HSDESWebServiceClient;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System.Collections.Generic;
    using System.Linq;


    [TestClass]
    public class LocalCacheTests
    {
        private Mock<IWebApiClient> CreateWebApiMock()
        {
            var mock = new Mock<IWebApiClient>();
            Responses<Result> resp = new Responses<Result>();
            resp.responses = new List<Response<Result>>();

            Dictionary<int, List<int>> existingIds = new Dictionary<int, List<int>>();
            existingIds[1] = new List<int>() { 5 };
            existingIds[2] = new List<int>() { 7 };
            existingIds[11] = new List<int>() { 8, 9 };
            mock.Setup(api => api.FetchExistingIds(It.IsAny<string>(), It.IsAny<IEnumerable<int>>())).Returns(
                (string s, IEnumerable<int> ie) => ie == null ? existingIds : existingIds.Where(kv => ie.Contains(kv.Key)).ToDictionary(kv => kv.Key, kv => kv.Value));

            return mock;
        }

        [TestMethod]
        public void TestLocalCacheDuplicates()
        {
            var api = CreateWebApiMock();
            LocalCache lc = new LocalCache(api.Object, string.Empty);
            lc.Init(null);
            Assert.AreEqual(-1, lc.GetHsdesId(11));
            Assert.AreEqual(-1, lc.GetHsdesId("11"));
            var dupl = lc.GetDuplicates(11);
            Assert.IsNotNull(dupl);
            Assert.AreEqual(2, dupl.Count);
            Assert.IsTrue(dupl.Contains(8));
            Assert.IsTrue(dupl.Contains(9));

            Assert.AreEqual(11, lc.GetClassicId(8));
            Assert.AreEqual(11, lc.GetClassicId(9));
        }

        public void TestPartialInitialization()
        {
            var api = CreateWebApiMock();
            LocalCache lc = new LocalCache(api.Object, string.Empty);
            lc.Init(new List<int>() { 1, 11 });
            Assert.AreEqual(-1, lc.GetHsdesId(11));
            Assert.AreEqual(-1, lc.GetHsdesId("11"));
            var dupl = lc.GetDuplicates(11);
            Assert.IsNotNull(dupl);
            Assert.AreEqual(2, dupl.Count);
            Assert.IsTrue(dupl.Contains(8));
            Assert.IsTrue(dupl.Contains(9));

            Assert.AreEqual(11, lc.GetClassicId(8));
            Assert.AreEqual(11, lc.GetClassicId(9));
            Assert.AreEqual(5, lc.GetHsdesId(1));
            Assert.AreEqual(5, lc.GetHsdesId("1"));
            Assert.AreEqual(-1, lc.GetHsdesId(2));
            Assert.AreEqual(-1, lc.GetHsdesId("2"));
        }

        [TestMethod]
        public void TestLocalCache()
        {
            var api = CreateWebApiMock();
            LocalCache lc = new LocalCache(api.Object, string.Empty);
            lc.Init(null);


            Assert.AreEqual(5, lc.GetHsdesId(1));
            Assert.AreEqual(5, lc.GetHsdesId("1"));
            Assert.AreEqual(7, lc.GetHsdesId(2));
            Assert.AreEqual(7, lc.GetHsdesId("2"));
            Assert.AreEqual(-1, lc.GetHsdesId(10));
            Assert.AreEqual(-1, lc.GetHsdesId("10"));

            Assert.AreEqual(1, lc.GetClassicId(5));
            Assert.AreEqual(1, lc.GetClassicId("5"));
            Assert.AreEqual(2, lc.GetClassicId(7));
            Assert.AreEqual(2, lc.GetClassicId("7"));
            Assert.AreEqual(-1, lc.GetClassicId(10));
            Assert.AreEqual(-1, lc.GetClassicId("10"));

            lc.AddIdsPair(3, 10);

            Assert.AreEqual(10, lc.GetHsdesId(3));
            Assert.AreEqual(10, lc.GetHsdesId("3"));

            Assert.AreEqual(3, lc.GetClassicId(10));
            Assert.AreEqual(3, lc.GetClassicId("10"));

            lc.AddIdsPair("4", "11");

            Assert.AreEqual(11, lc.GetHsdesId(4));
            Assert.AreEqual(11, lc.GetHsdesId("4"));

            Assert.AreEqual(4, lc.GetClassicId(11));
            Assert.AreEqual(4, lc.GetClassicId("11"));
        }
    }
}

