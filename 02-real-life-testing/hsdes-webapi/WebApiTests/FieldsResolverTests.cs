﻿namespace WebApiTests
{
    using HSDESWebApi;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;

    [TestClass]
    public class FieldsResolverTests
    {
        [TestMethod]
        public void TestFieldsResolver()
        {
            List<HSDESField> fields = new List<HSDESField>();
            fields.Add(new HSDESField("tenant.subject.myField", HSDESField.FieldKind.PlainText));
            fields.Add(new HSDESField("tenant.subject.uniqueField", HSDESField.FieldKind.PlainText));
            fields.Add(new HSDESField("title", HSDESField.FieldKind.PlainText));


            FieldsResolver r = new FieldsResolver(fields);

            Assert.IsNull(r.FindEsField("otherField"));
            Assert.IsNull(r.FindEsField(string.Empty));
            Assert.IsNull(r.FindEsField(null));

            var field = r.FindEsField("MYFIELD");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);

            field = r.FindEsField("myfield");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);

            field = r.FindEsField("myField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);

            field = r.FindEsField("tenant.subject.myField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);

            field = r.FindEsField("TeNaNt.SuBJeCT.MYField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);



            field = r.FindEsField("UNIQUEFIELD");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("uniquefield");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("uniqueField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("tenant.subject.uniqueField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("TeNaNt.SuBJeCT.uniQueField");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("TITLE");
            Assert.IsNotNull(field);
            Assert.AreEqual("title", field.FieldName);

            field = r.FindEsField("title");
            Assert.IsNotNull(field);
            Assert.AreEqual("title", field.FieldName);

            List<string> localNames = new List<string>() { "uniquefield", "title", "myfield" };

            try
            {
                r.FindLocalField("aa");
                Assert.Fail("Resolver should have thrown an exception.");
            }
            catch (ApplicationException) { }

            r.CacheFieldsMap(localNames);

            Assert.AreEqual("uniquefield", r.FindLocalField("tenant.subject.uniqueField"));
            Assert.AreEqual("title", r.FindLocalField("title"));

            Assert.AreEqual(null, r.FindLocalField("non_existing"));


            field = r.FindEsField("uniquefield");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.uniqueField", field.FieldName);

            field = r.FindEsField("title");
            Assert.IsNotNull(field);
            Assert.AreEqual("title", field.FieldName);

            field = r.FindEsField("myfield");
            Assert.IsNotNull(field);
            Assert.AreEqual("tenant.subject.myField", field.FieldName);
        }
    }
}
