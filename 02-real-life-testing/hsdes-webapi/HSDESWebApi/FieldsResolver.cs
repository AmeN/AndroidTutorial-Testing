﻿namespace HSDESWebApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class finding mappings from local field names to HSDES fields. Local field name can be mapped to a HSDES field when:
    /// <list type="bullet">
    /// <item>local name is equal to the name of the HSDES field, or</item>
    /// <item>HSDES field name's last segment (after last period) is equal to the local name.</item>
    /// </list>
    /// Names comparison is case-insensitive.
    /// </summary>
    public class FieldsResolver
    {
        /// <summary>
        /// Prefix of input data columns that describe fields containing HSDES id of other referenced articles (like "duplicate_id" for 
        /// articles duplicating others). Such values should be treated in a special way. Input contains Classic ID and output should
        /// be HSDES ID found based on the Classic ID>
        /// </summary>
        private const string REFERENCE_COLUMN_PREFIX = "__MAP";

        /// <summary>
        /// List of input data columns being reference fields, i.e. value from such column should be replaced by HSDES ID of record 
        /// having Classic ID equal to the input value.
        /// </summary>
        private List<string> referenceFields = new List<string>();

        /// <summary>
        /// Dictionary [local-column-name]=>[HSDES field name]
        /// </summary>
        private Dictionary<string, HSDESField> fieldMap;

        /// <summary>
        /// Collection of all HSDES fields being considered.
        /// </summary>
        private IEnumerable<HSDESField> remoteFields;

        /// <summary>
        /// List of input data columns being reference fields, i.e. value from such column should be replaced by HSDES ID of record 
        /// having Classic ID equal to the input value.
        /// </summary>
        public List<string> ReferenceFields
        {
            get
            {
                return referenceFields;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hsdEsFields">List of all HSDES fields to be considered.</param>
        public FieldsResolver(IEnumerable<HSDESField> hsdEsFields)
        {
            if (hsdEsFields == null)
            {
                throw new ArgumentException("HSDES Fields list client can't be null.", "hsdEsFields");
            }

            this.remoteFields = hsdEsFields;
        }

        /// <summary>
        /// Finds and memorizes mapping from given list of local field names to HSDES fields. 
        /// </summary>
        /// <param name="localColumns">List of all local field names to be used.</param>
        public void CacheFieldsMap(IList<string> localColumns)
        {
            this.fieldMap = CreateHSDESFieldsMap(remoteFields, localColumns);
        }

        /// <summary>
        /// Finds local field that maps to HSDES field of given name.
        /// </summary>
        /// <param name="hsdesFieldName">HSDES field full name, case-sensitive.</param>
        /// <returns>Respective local field name or null if not found.</returns>
        public string FindLocalField(string hsdesFieldName)
        {
            if (fieldMap == null)
            {
                throw new ApplicationException("Call \"CacheFieldsMap\" before invoking \"FindLocalField\" method!");
            }

            return fieldMap.Keys.Where(k => fieldMap[k].FieldName.Equals(hsdesFieldName)).FirstOrDefault();
        }

        /// <summary>
        /// Finds HSDES field that has name matching to given local name. Names comparison is case-insensitve. Names are matching
        /// when they equal to each other or HSDES field name ends with  "." + localName.
        /// </summary>
        /// <param name="localField">Local name to find HSDES field for</param>
        /// <returns>Respective HSDES field or null if no matching field found.</returns>
        public HSDESField FindEsField(string localField)
        {
            if (fieldMap != null)
            {
                return fieldMap.ContainsKey(localField) ? fieldMap[localField] : null;
            }

            if (string.IsNullOrEmpty(localField))
            {
                return null;
            }

            return remoteFields.Where(field => field.NameMatches(localField)).FirstOrDefault();
        }

        /// <summary>
        /// Create dictionary that contains map from local headers to HSDES field names.
        /// </summary> 
        /// <param name="hsdEsFields">List of hsd fields.</param>
        /// <param name="localHeaders">Local data column names.</param>
        /// <returns>Dictionary [local-column-name]=>[HSDES field name]</returns>
        private Dictionary<string, HSDESField> CreateHSDESFieldsMap(IEnumerable<HSDESField> hsdEsFields, IEnumerable<string> localHeaders)
        {
            Dictionary<string, HSDESField> ret = new Dictionary<string, HSDESField>();

            foreach (string column in localHeaders)
            {
                string colName = column;
                if (column.StartsWith(REFERENCE_COLUMN_PREFIX))
                {
                    var components = column.Split('.');
                    if (components.Length == 2)
                    {
                        colName = components[1];
                        this.referenceFields.Add(column);
                    }
                }
                foreach (HSDESField field in hsdEsFields)
                {
                    if (field.NameMatches(colName))
                    {
                        ret.Add(column, field);
                    }
                }
            }

            return ret;
        }
    }
}