﻿namespace HSDESWebApi
{
    using HSDESWebServiceClient;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class LocalCache
    {
        /// <summary>
        /// Web API client.
        /// </summary>
        private IWebApiClient client;

        /// <summary>
        ///  Map which maps classic IDs to respective HSD ES ids.
        /// </summary>
        private Dictionary<Int32, List<Int32>> existingRowsClassicToEs;

        /// <summary>
        ///  Map which maps HSDES IDs to respective HSD Classic ids.
        /// </summary>
        private Dictionary<Int32, Int32> existingRowsEsToClassic;

        /// <summary>
        /// Stores classic IDs that are duplicated in HSDES. Maps classic id to
        /// list of HSDES ids related to it.
        /// </summary>
        private Dictionary<Int32, List<Int32>> duplicatedClassicId = new Dictionary<Int32, List<Int32>>();

        /// <summary>
        /// Name of HSDES field containing classic ID.
        /// </summary>
        private string classicIdFieldName;

        /// <summary>
        /// A flag indicating whether this object has been initialized.
        /// </summary>
        public bool Initialized { get; private set; }

        /// <summary>
        /// All HSDES ids
        /// </summary>
        public IEnumerable<Int32> AllEsIds
        {
            get
            {
                return existingRowsEsToClassic == null ? null : existingRowsEsToClassic.Keys.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webClient">Web API client</param>
        /// <param name="classicIdField">Name of HSDES field containing classic ID.</param>
        public LocalCache(IWebApiClient webClient, string classicIdField)
        {
            if (webClient == null)
            {
                throw new ArgumentException("Client can't be null", "webClient");
            }

            client = webClient;
            classicIdFieldName = classicIdField;
            this.Initialized = false;
        }

        /// <summary>
        /// Downloads data from HSDES
        /// </summary>
        /// <param name="classicIds">List of classic Ids to work with.</param>
        public void Init(IEnumerable<int> classicIds)
        {
            this.existingRowsClassicToEs = client.FetchExistingIds(classicIdFieldName, classicIds);
            this.existingRowsEsToClassic = new Dictionary<int, int>();

            foreach (int classic in this.existingRowsClassicToEs.Keys)
            {
                if (this.existingRowsClassicToEs[classic].Count > 1)
                {
                    this.duplicatedClassicId[classic] = this.existingRowsClassicToEs[classic];
                    foreach (var esId in this.existingRowsClassicToEs[classic])
                    {
                        this.existingRowsEsToClassic[esId] = classic;
                    }
                }
                else
                {
                    this.existingRowsEsToClassic[this.existingRowsClassicToEs[classic][0]] = classic;
                }
            }

            this.Initialized = true;
        }

        /// <summary>
        /// Finds classic ID of article having specified HSDES id.
        /// </summary>
        /// <param name="hsdesId">HSDES id of an article.</param>
        /// <returns>Classic id of the article or -1 if article not found.</returns>
        public int GetClassicId(int hsdesId)
        {
            return existingRowsEsToClassic.ContainsKey(hsdesId) ? existingRowsEsToClassic[hsdesId] : -1;
        }

        /// <summary>
        /// Finds classic ID of article having specified HSDES id.
        /// </summary>
        /// <param name="hsdesId">HSDES id of an article.</param>
        /// <returns>Classic id of the article or -1 if article not found.</returns>
        public int GetClassicId(string hsdesId)
        {
            int parsed = int.Parse(hsdesId);
            return existingRowsEsToClassic.ContainsKey(parsed) ? existingRowsEsToClassic[parsed] : -1;
        }

        /// <summary>
        /// Gets all HSDES ids of records containing specified classicd if there is more than one.
        /// </summary>
        /// <param name="classicId">List of HSDES ids if specified classic ID exists in more than one HSDES record; otherwise, null</param>
        public List<int> GetDuplicates(int classicId)
        {
            return this.duplicatedClassicId.ContainsKey(classicId) ? this.duplicatedClassicId[classicId] : null;
        }

        /// <summary>
        /// Finds HSDES ID of article having specified Classic id.
        /// </summary>
        /// <param name="classicId">Classic id of an article.</param>
        /// <returns>HSDES id of the article or -1 if article not found.</returns>
        public int GetHsdesId(int classicId)
        {
            return existingRowsClassicToEs.ContainsKey(classicId) && existingRowsClassicToEs[classicId].Count == 1 ? existingRowsClassicToEs[classicId][0] : -1;
        }

        /// <summary>
        /// Finds HSDES ID of article having specified Classic id.
        /// </summary>
        /// <param name="classicId">Classic id of an article.</param>
        /// <returns>HSDES id of the article or -1 if article not found.</returns>
        public int GetHsdesId(string classicId)
        {
            return this.GetHsdesId(int.Parse(classicId));
        }

        /// <summary>
        /// Adds given Classic - HSDES id pair to the cache.
        /// </summary>
        /// <param name="classicId">Classic ID</param>
        /// <param name="hsdEsId">HSDES Id</param>
        public void AddIdsPair(int classicId, int hsdEsId)
        {
            if (!existingRowsClassicToEs.ContainsKey(classicId))
            {
                existingRowsClassicToEs[classicId] = new List<int>();
            }

            existingRowsClassicToEs[classicId].Add(hsdEsId);
            existingRowsEsToClassic[hsdEsId] = classicId;
        }

        /// <summary>
        /// Adds given Classic - HSDES id pair to the cache.
        /// </summary>
        /// <param name="classicIdString">Classic ID</param>
        /// <param name="hsdEsIdString">HSDES Id</param>
        public void AddIdsPair(string classicIdString, string hsdEsIdString)
        {
            var classicId = int.Parse(classicIdString);
            var hsdEsId = int.Parse(hsdEsIdString);
            this.AddIdsPair(classicId, hsdEsId);
        }
    }
}
