﻿namespace HSDESWebServiceClient
{
    using HSDESWebApi;
    using HSDESWebApi.Messages;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;

    public interface IWebApiClient
    {
        /// <summary>
        /// Sends POST HSD ES web api request.
        /// </summary>
        /// <param name="postData">String representation of a JSON request data.</param>
        /// 
        /// <param name="timeout">Optional: request timeout in milliseconds [default: 20000]</param>
        /// <returns>Response received from the Web API, deserialized from JSON.</returns>
        Responses<T> SendRequest<T>(string postData, int timeout = 20000) where T : Result;

        /// <summary>
        /// Fetches from the server list of existing classic ID values. Processes only classic
        /// ids from the specified list.
        /// </summary>
        /// <param name="uniqueFieldName">Name of HSD ES field containing classic ID</param>
        /// <param name="classicIds">List of classic IDs to process. Only classic Id values
        /// from the list will be downloaded.</param>
        /// 
        /// <returns>
        /// Map which maps classic IDs to respective lists of HSD ES ids for records containing given classic id.
        /// </returns>
        Dictionary<Int32, List<Int32>> FetchExistingIds(string uniqueFieldName, IEnumerable<int> classicIds);

        /// <summary>
        /// Fetches from the server list of existing Lookups from given tenant.
        /// </summary>
        /// <param name="lookupNameField">Field containing lookup name</param>
        /// <param name="lookupValueField">Field containing lookup value.</param>
        /// <param name="givenTenant">Tenant in which search should be performed.</param>
        /// 
        /// <returns>
        /// Map of pairs of lookup names and their values.
        /// </returns>
        List<JObject> FetchExistingLookups(string lookupNameField, string lookupValueField, string givenTenant, string givenSubject, List<string> additionalColumns);

        /// <summary>
        /// Downloads a list of all HSDES fields for given tenant-subject pair.
        /// </summary>
        /// <param name="tenant">Tenant name.</param>
        /// <param name="subject">Subject name.</param>
        /// <returns>Collection of HSDESField objects.</retur
        IEnumerable<HSDESField> GetFieldsList(string tenant, string subject);

        /// <summary>
        /// Prints out to console results information regarding the Web API request.
        /// </summary>
        /// <typeparam name="T">Type of response</typeparam>
        /// <param name="resp">Response received.</param>
        /// <param name="errorMsg">Message to print on request failure</param>
        /// <param name="successMsg">Message to print on request success</param>
        void LogResultStatus<T>(Responses<T> resp, string errorMsg, string successMsg) where T : Result;
    }
}
