﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class InsertLookupValueArgs : BaseArgs
    {
        public string lookup_group { get; set; }
        public string value { get; set; }
        public int order { get; set; }
        public string description { get; set; }
    }
}
