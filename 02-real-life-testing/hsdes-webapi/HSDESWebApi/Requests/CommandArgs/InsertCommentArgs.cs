﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class InsertCommentArgs:BaseArgs
    {
        public string tenant { get; set; }
        public string parent_id { get; set; }
    }
}
