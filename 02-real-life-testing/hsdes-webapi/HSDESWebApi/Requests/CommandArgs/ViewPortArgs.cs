﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class ViewPortArgs:BaseArgs
    {
        public string viewPortName { get; set; }
    }
}
