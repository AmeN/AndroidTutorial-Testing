﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class SubjectTenantFieldArgs : SubjectTenantArgs
    {
        public string fieldName { get; set; }
    }
}
