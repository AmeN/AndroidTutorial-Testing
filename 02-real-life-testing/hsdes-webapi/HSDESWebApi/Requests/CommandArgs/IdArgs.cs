﻿namespace HSDESWebApi.Requests.CommandArgs
{
    public class IdArgs : BaseArgs
    {
        public long id { get; set; }
    }
}
