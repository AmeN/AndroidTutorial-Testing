﻿namespace HSDESWebApi.Requests.CommandArgs
{
    public class DirectoryInfoArgs : BaseArgs
    {
        public string search_string { get; set; }
        public string check_exact_match { get; set; }
    }
}
