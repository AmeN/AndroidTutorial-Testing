﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class SubjectTenantOffsetCount : SubjectTenantArgs
    {
        public int offset { get; set; }
        public int count { get; set; }
    }
}