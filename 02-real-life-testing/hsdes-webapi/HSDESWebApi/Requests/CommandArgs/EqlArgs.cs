﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class EqlArgs : BaseArgs
    {
        public string eql { get; set; }
        public int offset { get; set; }
        public int count { get; set; }
    }
}
