﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class SubjectTenantArgs : BaseArgs
    {
        public string subject { get; set; }
        public string tenant { get; set; }
    }
}
