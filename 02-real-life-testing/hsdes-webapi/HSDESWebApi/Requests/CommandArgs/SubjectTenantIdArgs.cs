﻿
namespace HSDESWebApi.Requests.CommandArgs
{
    public class SubjectTenantIdArgs: SubjectTenantArgs
    {
        public string id { get; set; }
    }
}
