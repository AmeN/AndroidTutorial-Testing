﻿namespace HSDESWebApi.Requests.CommandArgs
{
    public class CloneRecordArgs : SubjectTenantIdArgs
    {
        public string dest_subject { get; set; }
        public string dest_tenant { get; set; }
        public string copy_attachment { get; set; }
        public string copy_comment { get; set; }
    }
}
