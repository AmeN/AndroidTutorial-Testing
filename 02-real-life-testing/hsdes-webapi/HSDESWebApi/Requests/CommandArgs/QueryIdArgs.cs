﻿namespace HSDESWebApi.Requests.CommandArgs
{
    public class QueryIdArgs: BaseArgs
    {
        public long query_id { get; set; }
        public int offset { get; set; }
        public int count { get; set; }
    }
}
