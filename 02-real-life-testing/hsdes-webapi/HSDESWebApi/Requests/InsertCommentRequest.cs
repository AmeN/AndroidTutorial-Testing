﻿using HSDESWebApi.Requests.CommandArgs;
using System;

namespace HSDESWebApi.Requests
{
    /// <summary>
    /// HSD ES Web API command data for inserting comment to an article.
    /// </summary>
    public class InsertCommentRequest : ParametrizedRequest<InsertCommentArgs>
    {
        public InsertCommentRequest()
            : base()
        {
            command = "insert_comment";
        }

        public void SetAuthor(String author)
        {
            this.AddVarArg("owner", author);
            this.AddVarArg("submitted_by", author);
        }

        public void SetContents(String contents)
        {
            this.AddVarArg("description", contents);
        }

        public void SetSubmittedDate(DateTime submittedDate)
        {
            this.AddVarArg("submitted_date", submittedDate.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
