﻿using HSDESWebApi.Requests.CommandArgs;

namespace HSDESWebApi.Requests
{
    public class ParametrizedRequest<T> : Request where T : BaseArgs, new()
    {
        public T command_args { get; set; }
        public ParametrizedRequest()
        {
            this.command_args = new T();
        }
    }
}
