﻿namespace HSDESWebApi.Requests
{
    using HSDESWebApi.Requests.CommandArgs;
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Text;
    using System.Linq;

    public class Request
    {
        public string tran_id { get; set; }
        public string command { get; protected set; }
        public List<ExpandoObject> var_args { get; set; }
        public List<ExpandoObject> copy_args { get; set; }
        protected Request()
        {
            var_args = new List<ExpandoObject>();
            copy_args = new List<ExpandoObject>();
            tran_id = Guid.NewGuid().ToString();
            this.AddVarArg("send_mail", "false");
        }

        /// <summary>
        /// Adds new var arg.
        /// </summary>
        /// <param name="key">Name of field.</param>
        /// <param name="value">Value of field.</param>
        public void AddVarArg(string key, object value)
        {
            IDictionary<string, Object> obj = new ExpandoObject() as IDictionary<string, Object>;
            obj.Add(key, value);
            var_args.Add(obj as ExpandoObject);
        }

        public void AddCopyArgs(string key, object value)
        {
            IDictionary<string, Object> obj = new ExpandoObject() as IDictionary<string, Object>;
            obj.Add(key, value);
            copy_args.Add(obj as ExpandoObject);
        }

        #region Factory methods

        public static ParametrizedRequest<EqlArgs> CreateGetChangesetsForBugRequest(long bugId, int count = int.MaxValue, int offset = 0)
        {
            StringBuilder eql = new StringBuilder();
            eql.Append("select status, parent_id where parent_id in(").Append(bugId).Append(")");
            return CreateEqlRequest(eql.ToString(), count, offset);
        }

        public static ParametrizedRequest<EqlArgs> CreateGetChangesetsRequest(ICollection<long> ids, int count = int.MaxValue, int offset = 0)
        {
            if (ids == null || ids.Count == 0) throw new ArgumentException("List of changeset IDs can't be null or empty", "ids");
            StringBuilder eql = new StringBuilder();

            eql.Append("select status, parent_id where id in(");
            foreach(var id in ids.Take(ids.Count-1))
            {
                eql.Append(id).Append(",");
            }

            eql.Append(ids.Last()).Append(")");
            return CreateEqlRequest(eql.ToString(), count, offset);
        }

        public static ParametrizedRequest<EqlArgs> CreateEqlRequest(string eql, int count, int offset = 0)
        {
            var ret = new ParametrizedRequest<EqlArgs>();
            ret.command = "get_records_by_eql";
            ret.command_args.eql = eql;
            ret.command_args.count = count;
            ret.command_args.offset = offset;
            return ret;
        }

        public static ParametrizedRequest<CloneRecordArgs> CreateCloneRecordRequest(string dest_tenant, string dest_subject, bool copy_attachment, bool copy_comment)
        {
            var ret = new ParametrizedRequest<CloneRecordArgs>();
            ret.command = "clone";
            ret.command_args.dest_tenant = dest_tenant;
            ret.command_args.dest_subject = dest_subject;
            ret.command_args.copy_attachment = copy_attachment.ToString();
            ret.command_args.copy_comment = copy_comment.ToString();
            return ret;
        }

        public static ParametrizedRequest<QueryIdArgs> CreateGetRecordsFromQueryRequest(int queryId, int offset, int count)
        {
            var ret = new ParametrizedRequest<QueryIdArgs>();
            ret.command = "get_records_by_query_id";
            ret.command_args.query_id = queryId;
            ret.command_args.offset = offset;
            ret.command_args.count = count;
            return ret;
        }

        public static ParametrizedRequest<SubjectTenantFieldArgs> CreateFetchLookupValuesRequest(string tenant, string subject, string fieldName)
        {
            var ret = new ParametrizedRequest<SubjectTenantFieldArgs>();
            ret.command = "get_lookup_values";
            ret.command_args.tenant = tenant;
            ret.command_args.subject = subject;
            ret.command_args.fieldName = fieldName;
            return ret;
        }

        public static ParametrizedRequest<InsertLookupValueArgs> CreateInsertLookupValueRequest(string group, string value, int order = 0, string description = null)
        {
            var ret = new ParametrizedRequest<InsertLookupValueArgs>();
            ret.command = "insert_lookup_value";
            ret.command_args.lookup_group = group;
            ret.command_args.value = value;
            ret.command_args.order = order;
            ret.command_args.description = description;
            return ret;
        }

        public static ParametrizedRequest<IdArgs> CreateFetchRecordByIdRequest(long id)
        {
            var ret = new ParametrizedRequest<IdArgs>();
            ret.command = "get_record_by_id";
            ret.command_args.id = id;
            return ret;
        }

        public static ParametrizedRequest<DirectoryInfoArgs> CreateGetDirectoryInfoRequest(string person, bool exact_match)
        {
            var req = new ParametrizedRequest<DirectoryInfoArgs>();

            req.command = "get_directory_info";
            req.command_args.search_string = person;
            req.command_args.check_exact_match = exact_match ? "y" : "n";

            return req;
        }

        public static ParametrizedRequest<SubjectTenantArgs> CreateFetchRulesRequest(string tenant, string subject)
        {
            return CreateParametrizedRequest(tenant, subject, "get_viewport_ui_rules");
        }

        public static ParametrizedRequest<SubjectTenantIdArgs> CreateFetchCommentsRequest(string id, string tenant, string subject)
        {
            return CreateParametrizedRequest(id, tenant, subject, "get_comments_by_record_id");
        }

        public static ParametrizedRequest<SubjectTenantIdArgs> CreateUpdateRequest(string id, string tenant, string subject)
        {
            return CreateParametrizedRequest(id, tenant, subject, "update_record");
        }

        public static ParametrizedRequest<SubjectTenantArgs> CreateInsertRequest(string tenant, string subject)
        {
            return CreateParametrizedRequest(tenant, subject, "insert_record");
        }

        public static ParametrizedRequest<SubjectTenantArgs> CreateInsertWithFetchRequest(string tenant, string subject, string parent_id)
        {
            var ret =  CreateParametrizedRequest(tenant, subject, "insert_record_with_fetch");
            ret.AddCopyArgs("parent_id", parent_id);
            return ret;
        }

        public static ParametrizedRequest<SubjectTenantArgs> CreateGetFieldsRequest(string tenant, string subject)
        {
            return CreateParametrizedRequest(tenant, subject, "get_viewport_all_field_info");
        }

        public static ParametrizedRequest<TenantArgs> CreateGetSubjectsRequest(string tenant)
        {
            var ret = new ParametrizedRequest<TenantArgs>();
            ret.command = "get_tenant_subject_list";
            ret.command_args.tenant = tenant;
            return ret;
        }

        public static Request CreateGetPermissionViewportRequest(string viewPortName)
        {
            var ret = new ParametrizedRequest<ViewPortArgs>();
            ret.command = "get_permission_viewport_execute";
            ret.command_args.viewPortName = viewPortName;
            return ret;
        }

        public static ParametrizedRequest<SubjectTenantOffsetCount> CreateGetRecordsRequest(string tenant, string subject)
        {
            var ret = new ParametrizedRequest<SubjectTenantOffsetCount>();
            ret.command = "get_records";
            ret.command_args.tenant = tenant;
            ret.command_args.subject = subject;
            return ret;
        }

        public static ParametrizedRequest<SubjectTenantIdArgs> CreateGetAttachmentListRequest(string tenant, string subject, string articleId)
        {
            var ret = new ParametrizedRequest<SubjectTenantIdArgs>();
            ret.command = "get_attachment_list";
            ret.command_args.tenant = tenant;
            ret.command_args.subject = subject;
            ret.command_args.id = articleId;
            return ret;
        }

        private static ParametrizedRequest<SubjectTenantArgs> CreateParametrizedRequest(string tenant, string subject, string command)
        {
            var ret = new ParametrizedRequest<SubjectTenantArgs>();
            ret.command = command;
            ret.command_args.tenant = tenant;
            ret.command_args.subject = subject;
            return ret;
        }

        private static ParametrizedRequest<SubjectTenantIdArgs> CreateParametrizedRequest(string id, string tenant, string subject, string command)
        {
            var ret = new ParametrizedRequest<SubjectTenantIdArgs>();
            ret.command = command;
            ret.command_args.id = id;
            ret.command_args.tenant = tenant;
            ret.command_args.subject = subject;
            return ret;
        }
        #endregion
    }
}
