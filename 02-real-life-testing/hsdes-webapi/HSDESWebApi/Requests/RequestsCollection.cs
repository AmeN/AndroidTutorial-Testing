﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace HSDESWebApi.Requests
{
    public class RequestsCollection
    {
        public RequestsCollection()
        {
            requests = new List<Request>();
        }

        public RequestsCollection(Request r)
        {
            requests = new List<Request>();
            requests.Add(r);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public List<Request> requests { get; set; }
    }
}
