﻿namespace HSDESWebServiceClient
{
    using HSDDataCommon;
    using HSDESWebApi;
    using HSDESWebApi.Messages;
    using HSDESWebApi.Requests;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;

    /// <summary>
    /// Client class, interacting with HSDES Web API.
    /// </summary>
    public class Client : IWebApiClient
    {
        /// <summary>
        /// Url of the Web API endpoint
        /// </summary>
        private string url;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="url">Url of the Web API endpoint</param>
        public Client(string url)
        {
            this.url = url;
        }

        /// <summary>
        /// Sends POST HSD ES web api request.
        /// </summary>
        /// <param name="postData">String representation of a JSON request data.</param>
        /// <param name="uri">URI of the Web API</param>
        /// <param name="timeout">Optional: request timeout in milliseconds [default: 20000]</param>
        /// <returns>Response received from the Web API, deserialized from JSON.</returns>
        public Responses<T> SendRequest<T>(string postData, int timeout = 20000) where T : Result
        {
            string ret = SubmitJSONThruWebAPI(postData, timeout);
            var resp = JsonConvert.DeserializeObject<Responses<T>>(ret);
            resp.RawResponse = ret;
            return resp;
        }

        /// <summary>
        /// Fetches from the server list of existing classic ID values. Processes only classic
        /// ids from the specified list.
        /// </summary>
        /// <param name="uniqueFieldName">Name of HSD ES field containing classic ID</param>
        /// <param name="classicIds">List of classic IDs to process. Only classic Id values
        /// from the list will be downloaded. If set to null, all classic Ids will be processed.</param>
        /// 
        /// <returns>
        /// Map which maps classic IDs to respective lists of HSD ES ids for records containing given classic id.
        /// </returns>
        public Dictionary<Int32, List<Int32>> FetchExistingIds(string uniqueFieldName, IEnumerable<int> classicIds)
        {
            var ret = new Dictionary<Int32, List<Int32>>();

            string eql = "";
            if (classicIds == null)
            {
                eql = string.Format("select id, {0} where {0}  !=\"\"", uniqueFieldName);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("select id, {0} where {0} in (", uniqueFieldName);
                bool any = false;
                foreach (int id in classicIds)
                {
                    sb.AppendFormat("{0}, ", id);
                    any = true;
                }

                if (any)
                {
                    sb.Remove(sb.Length - 2, 2);
                }

                sb.Append(")");
                eql = sb.ToString();
            }

            var response = SendRequest<Result>(new RequestsCollection(Request.CreateEqlRequest(eql, int.MaxValue)).ToJson());
            JObject json = response.JsonResponse;
            if (response.Success)
            {
                if (response.responses[0].result_table.Count > 0)
                {
                    var existingRows = json["responses"][0]["result_table"].Select(v => new
                    {
                        Key = int.Parse(v[uniqueFieldName].ToString()),
                        Val = int.Parse(v["id"].ToString())
                    }).ToList();

                    foreach (var item in existingRows)
                    {
                        if (!ret.ContainsKey(item.Key))
                        {
                            ret[item.Key] = new List<int>();
                        }

                        ret[item.Key].Add(item.Val);
                    }

                }
            }

            return ret;
        }

        public List<JObject> FetchExistingLookups(string lookupNameField, string lookupValueField, string givenTenant, string givenSubject, List<string> additionalColumns)
        {
            var ret = new List<JObject>();

            // build additional columns string
            StringBuilder builder = new StringBuilder("  , ");
            string eql_additional_columns = "";
            foreach (string singleColumn in additionalColumns)
            {
                builder.Append(givenSubject);
                builder.Append(".");
                builder.Append(singleColumn);
                builder.Append(" , ");
            }

            // remove last three characters which should be (if any column has been added) ' , '
            eql_additional_columns = builder.ToString().Substring(0, builder.Length - 3);

            string eql = " select id , " + lookupNameField + " , " + lookupValueField + eql_additional_columns + " where " + lookupNameField + " != '' and " + lookupValueField + " != '' and tenant = '" + givenTenant + "'";

            var response = SendRequest<Result>(new RequestsCollection(Request.CreateEqlRequest(eql, int.MaxValue)).ToJson());
            JObject json = response.JsonResponse;
            if (response.Success)
            {
                if (response.responses[0].result_table.Count > 0)
                {
                    ret = json["responses"][0]["result_table"].Select(v => v.Value<JObject>()).ToList();
                }
            }

            return ret;
        }

        /// <summary>
        /// Sends given string to HSD ES web API.
        /// </summary>
        /// <param name="jsonData">String to be sent to HSD ES.</param>
        /// 
        /// <param name="timeout">Optional milliseconds timeout. [Default: 20000]</param>
        /// <returns>Received response as string. Probably it is JSON data string.</returns>
        private string SubmitJSONThruWebAPI(string jsonData, int timeout = 20000)
        {
            string ret = string.Empty;
            StreamWriter requestWriter;
            var svcUri = new Uri(url);
            var productsUri = new Uri(svcUri, "ESService");
            var req = System.Net.WebRequest.Create(productsUri) as HttpWebRequest;
            req.Credentials = CredentialCache.DefaultCredentials;
            if (req != null)
            {
                req.Method = "POST";
                req.ServicePoint.Expect100Continue = false;
                req.Timeout = timeout;
                req.ContentType = "application/json";
                using (requestWriter = new StreamWriter(req.GetRequestStream()))
                {
                    requestWriter.Write(jsonData);
                }
            }

            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                using (Stream resStream = resp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(resStream);
                    ret = reader.ReadToEnd();
                }
            }

            return ret;
        }

        /// <summary>
        /// Downloads a list of all HSDES fields for given tenant-subject pair.
        /// </summary>
        /// <param name="tenant">Tenant name.</param>
        /// <param name="subject">Subject name.</param>
        /// <returns>Collection of HSDESField objects.</returns>
        public IEnumerable<HSDESField> GetFieldsList(string tenant, string subject)
        {
            Logging.LogInfo(string.Format("Fetching fields for {0}.{1} article object.", tenant, subject));
            var resp = this.SendRequest<FieldResult>(new RequestsCollection(Request.CreateGetFieldsRequest(tenant, subject)).ToJson());
            this.LogResultStatus<FieldResult>(resp, "Error downloading fields list", "Fields fetch successful.");

            if (resp.Success)
            {
                return resp.responses[0].result_table.Select(r => new HSDESField(r.Name, HSDESField.ParseKind(r.Kind))).ToList();
            }
            else
            {
                throw new ApplicationException("Fields list could not be downloaded.");
            }
        }

        /// <summary>
        /// Prints out to console results information regarding the Web API request.
        /// </summary>
        /// <typeparam name="T">Type of response</typeparam>
        /// <param name="resp">Response received.</param>
        /// <param name="errorMsg">Message to print on request failure</param>
        /// <param name="successMsg">Message to print on request success</param>
        public void LogResultStatus<T>(Responses<T> resp, string errorMsg, string successMsg) where T : Result
        {
            foreach (var response in resp.responses)
            {
                if (response.status != "success")
                {
                    Logging.LogError("Transaction id is " + response.tran_id);

                    if (response.result_params != null)
                    {
                        if (!string.IsNullOrEmpty(response.result_params.newId))
                        {
                            Logging.LogError("NewId is " + response.result_params.newId);
                        }
                        if (!string.IsNullOrEmpty(response.result_params.updated_id))
                        {
                            Logging.LogError("Updated_id is " + response.result_params.updated_id);
                        }
                    }
                    Logging.LogError(errorMsg);
                    Logging.LogError("Messages:");

                    foreach (Message msg in response.messages)
                    {
                        Logging.LogError("\tCode: " + msg.errorcode);
                        Logging.LogError("\tDetails: " + msg.errordetail);
                        Logging.LogError("\tSummary: " + msg.errorsummary);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(successMsg))
                    {
                        if (!string.IsNullOrEmpty(response.result_params.newId))
                        {
                            Logging.LogInfo("NewId is " + response.result_params.newId);
                        }
                        if (!string.IsNullOrEmpty(response.result_params.updated_id))
                        {
                            Logging.LogInfo("Updated_id is " + response.result_params.updated_id);
                        }

                        Logging.LogInfo(successMsg);
                    }
                }
            }
        }
    }
}
