﻿namespace HSDESWebApi
{

    public class HSDESField
    {
        public enum FieldKind
        {
            Unknown,
            RichText,
            PlainText,
            People,
            Person,
            SingleHyperlink
        }

        public static FieldKind ParseKind(string textKind)
        {
            switch (textKind.ToLower())
            {
                case "rich text":
                    return FieldKind.RichText;

                case "plain text":
                    return FieldKind.PlainText;
                case "person":
                    return FieldKind.Person;
                case "people":
                    return FieldKind.People;
                case "single-hyperlink":
                    return FieldKind.SingleHyperlink;

                default:
                    return FieldKind.Unknown;
            }
        }

        public HSDESField()
        {

        }

        public HSDESField(string name, FieldKind kind)
        {
            this.FieldName = name;
            this.Kind = kind;
        }

        /// <summary>
        /// Field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Kind.
        /// </summary>
        public FieldKind Kind { get; set; }

        /// <summary>
        /// Checks if this field's name equals or ends with given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool NameMatches(string name)
        {
            string myName = FieldName.ToLower();
            name = name.ToLower();
            return myName.Equals(name) || myName.EndsWith("." + name);
        }
    }
}
