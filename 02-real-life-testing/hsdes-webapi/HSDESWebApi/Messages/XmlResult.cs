﻿namespace HSDESWebApi.Messages
{
    /// <summary>
    /// Result of "Get Viewport UI Rules" HSDES Web API request. 
    /// </summary>
    public class XmlResult : Result
    {
        public string xml { get; set; }
    }
}
