﻿namespace HSDESWebApi.Messages
{
    /// <summary>
    /// Error message returned from HSDES Web API
    /// </summary>
    public class Message
    {
        public string errorcode { get; set; }
        public string errorsummary { get; set; }
        public string errordetail { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] {1} : {2}", errorcode, errorsummary, errordetail);
        }
    }
}