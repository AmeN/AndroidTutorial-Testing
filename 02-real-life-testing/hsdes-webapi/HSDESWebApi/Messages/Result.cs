﻿namespace HSDESWebApi.Messages
{
    /// <summary>
    /// Result of a single HSDES Web API request
    /// </summary>
    public class Result
    {
        public string id { get; set; }
        public string title { get; set; }
    }
}