﻿namespace HSDESWebApi.Messages
{
    using System;

    /// <summary>
    /// Result of "Get Comments" HSDES Web API request. 
    /// </summary>
    public class CommentResult : Result
    {
        public string submitted_by { get; set; }
        public DateTime submitted_date { get; set; }
        public string description { get; set; }
    }
}