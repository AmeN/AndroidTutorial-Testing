﻿namespace HSDESWebApi.Messages
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Response to a single HSDES Web API request. May contain zero or more results (like many records returned by single query)
    /// and/or zero or more error messages.
    /// </summary>
    /// <typeparam name="T">Type of result returned by the request.</typeparam>
    public class Response<T> where T : Result
    {
        public string tran_id { get; set; }
        public string status { get; set; }
        public IList<Message> messages { get; set; }
        public Params result_params { get; set; }
        public IList<T> result_table { get; set; }

        [JsonIgnore]
        public bool Success
        {
            get
            {
                return "success".Equals(status.ToLower());
            }
        }
    }
}
