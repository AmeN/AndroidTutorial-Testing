﻿namespace HSDESWebApi.Messages
{
    using Newtonsoft.Json;

    public class LookupResult : Result
    {
        [JsonProperty("sys_lookup.value")]
        public string value { get; set; }

        [JsonProperty("sys_lookup.order")]
        public int order { get; set; }

        public string description { get; set; }

        [JsonProperty("sys_lookup.acceptable_values")]
        public string acceptable_values { get; set; }

        [JsonProperty("sys_lookup.lookup_group")]
        public string lookup_group { get; set; }

        [JsonProperty("sys_lookup.filter_value")]
        public string filter_value { get; set; }
    }
}
