﻿namespace HSDESWebApi.Messages
{
    using Newtonsoft.Json;

    public class FieldResult : Result
    {
        [JsonProperty("field")]
        public string Name { get; set; }

        [JsonProperty("logical_field_type")]
        public string LogicalType { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }
    }
}
