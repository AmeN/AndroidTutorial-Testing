﻿namespace HSDESWebApi.Messages
{
    using Newtonsoft.Json;

    public class ChangesetResult : Result
    {
        [JsonProperty("parent_id")]
        public long ParentId { get; set; }
        
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
