﻿namespace HSDESWebApi.Messages
{
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Text;
    /// <summary>
    /// List of responses returned by HSDES Web API for multiple requests sent in a single JSON HTTP POST.
    /// </summary>
    /// <remarks>
    /// Be aware that this implementation assumes that all requests produce the same kind of response. This is limitation
    /// in comparison to pure Web API, which allows to combine many requests of various types and respones.
    /// </remarks>
    /// <typeparam name="T">Type of result expected from each of the included requests.</typeparam>
    public class Responses<T> where T : Result
    {
        public string RawResponse { get; set; }

        public IList<Response<T>> responses { get; set; }

        public JObject JsonResponse
        {
            get
            {
                return JObject.Parse(RawResponse);
            }
        }

        /// <summary>
        /// Checks if there is at least one not-null response and if first response has "Success" status.
        /// </summary>
        public bool Success
        {
            get
            {
                return responses != null && responses.Count > 0 && responses[0].Success;
            }
        }

        /// <summary>
        /// Checks if all responses has there is at least one not-null response and if first response has "Success" status.
        /// </summary>
        public bool AllSuccess
        {
            get
            {
                var allSuccess = true;

                foreach (var response in responses)
                {
                    allSuccess = allSuccess && response.Success;
                }

                return responses != null && responses.Count > 0 && allSuccess;
            }
        }

        /// <summary>
        /// Checks if there is at least one not-null response and if at least one of them has "Success" status.
        /// </summary>
        public bool PartialSuccess
        {
            get
            {

                if (responses == null || responses.Count == 0)
                {
                    return false;
                }

                foreach (var response in responses)
                {
                    if (response.Success)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Returns list of all error messages separated by new line.
        /// </summary>
        public string AllMessages
        {
            get
            {
                StringBuilder messages = new StringBuilder();

                foreach (var response in responses)
                {
                    if (!response.Success)
                    {
                        messages.Append(response.messages[0].ToString()).Append(System.Environment.NewLine);
                    }
                }

                return messages.ToString();
            }
        }

        /// <summary>
        /// Checks if given responses collection object is null or contains no responses inside.
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="resp"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<U>(Responses<U> resp) where U : Result
        {
            return resp == null || resp.responses == null || resp.responses.Count == 0;
        }
    }
}
