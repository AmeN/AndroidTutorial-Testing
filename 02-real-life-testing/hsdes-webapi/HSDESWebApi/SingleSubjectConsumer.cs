﻿namespace HSDESWebApi
{
    using HSDESWebServiceClient;
    using System;

    /// <summary>
    /// Base class for classes consuming HSDEDS Web API and working on single tenant-subject.
    /// </summary>
    public abstract class SingleSubjectConsumer: AbstractWebApiConsumer
    {
        /// <summary>
        /// Name of the HSDES tenant to work on.
        /// </summary>
        protected string tenantName;

        /// <summary>
        /// Name of the HSDES subiect to work on.
        /// </summary>
        protected string subjectName;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenant"> Name of the HSDES tenant to work on.</param>
        /// <param name="subject">Name of the HSDES subiect to work on.</param>
        public SingleSubjectConsumer(IWebApiClient webClient, string tenant, string subject)
            :base(webClient)
        {
            if (string.IsNullOrEmpty(tenant)) throw new ArgumentException("Tenant name can't be null or empty", "tenant");
            if (string.IsNullOrEmpty(subject)) throw new ArgumentException("Subject name can't be null or empty", "tenant");

            this.tenantName = tenant;
            this.subjectName = subject;
        }
    }
}
