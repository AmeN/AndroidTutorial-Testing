﻿namespace HSDESWebApi
{
    using HSDESWebServiceClient;
    using System;

    /// <summary>
    /// Base class for all classes consuming HSD ES Web API
    /// </summary>
    public abstract class AbstractWebApiConsumer
    {
        /// <summary>
        /// Web API client
        /// </summary>
        protected IWebApiClient client;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webClient">Web API client</param>
        public AbstractWebApiConsumer(IWebApiClient webClient)
        {
            if (webClient == null) throw new ArgumentException("Web client can't be null", "webClient");
            this.client = webClient;
        }
    }
}
